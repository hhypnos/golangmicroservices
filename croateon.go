package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// Define the target API endpoint
	targetURL, _ := url.Parse("http://207.154.220.165:9090/")

	// Create a reverse proxy
	proxy := httputil.NewSingleHostReverseProxy(targetURL)

	// CORS middleware
	r.Use(func(c *gin.Context) {
		origin := c.Request.Header.Get("Origin")
		fmt.Println(origin)
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", origin)
			c.Header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			c.Header("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept")
			c.Header("Access-Control-Allow-Credentials", "true") // Include this header for requests with credentials
			c.Header("Access-Control-Max-Age", "86400")

			if c.Request.Method == "OPTIONS" {
				c.AbortWithStatus(http.StatusOK)
				return
			}
		}

		c.Next()
	})

	// Intercept middleware
	r.Use(func(c *gin.Context) {
		// Check if the request is likely from Postman
		if isLikelyPostmanRequest(c.Request) {
			c.JSON(http.StatusForbidden, gin.H{"error": "Access forbidden from Postman"})
			return
		}
		// Clone the incoming request
		req := c.Request.Clone(c.Request.Context())

		// Forward the cloned request to the real API endpoint
		proxy.ServeHTTP(c.Writer, req)
	})

	// Run server
	r.Run(":8080")
}

// isLikelyPostmanRequest checks if the request is likely made from Postman
func isLikelyPostmanRequest(req *http.Request) bool {
	// Check if the request is made by a browser
	userAgent := req.Header.Get("User-Agent")
	if strings.Contains(userAgent, "Mozilla") || strings.Contains(userAgent, "AppleWebKit") {
		return false
	}

	// Check if the Referer header is empty or not from the same origin
	referer := req.Header.Get("Referer")
	if referer == "" || !strings.HasPrefix(referer, req.Host) {
		return true
	}

	// Check if the Accept header is missing or doesn't contain "text/html"
	acceptHeader := req.Header.Get("Accept")
	if acceptHeader == "" || !strings.Contains(acceptHeader, "text/html") {
		return true
	}

	// Check if the request method is not GET or POST
	if req.Method != http.MethodGet && req.Method != http.MethodPost {
		return true
	}

	// Check if specific session cookies are missing
	sessionCookie := req.Header.Get("Cookie")
	if !strings.Contains(sessionCookie, "session_id=") {
		return true
	}

	// Check for other known API client headers
	if req.Header.Get("Postman-Token") != "" || req.Header.Get("X-Postman-Id") != "" {
		return true
	}

	// Check for unusual or missing content length
	if req.ContentLength == 0 || req.ContentLength > 1024 {
		return true
	}

	// Check for other known API client headers
	if req.Header.Get("Postman-Token") != "" || req.Header.Get("X-Postman-Id") != "" {
		return true
	}

	// Add more checks as needed based on your application's requirements

	return false
}
