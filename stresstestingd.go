package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	var z = 0
	apiURL := "https://api.ipify.org?format=text"
	response, err := http.Get(apiURL)
	if err != nil {
		fmt.Println("Error fetching IP:", err)
		return
	}
	defer response.Body.Close()

	ip, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading IP response:", err)
		return
	}

	fmt.Println("Your public IP address is:", string(ip))
	for {
		baseAPIURL := ""
		numRequests := 1000
		delay := time.Millisecond / 5 // Sending 5 requests per millisecond
		rand.Seed(time.Now().UnixNano())
		min := 10000
		max := 99999
		for i := 1; i <= numRequests; i++ {
			requestURL := fmt.Sprintf("%s%d", baseAPIURL, rand.Intn(max-min+1)+min)

			// Print the request URL for debugging (you can comment this line out)
			fmt.Println("Sending request to:", requestURL)

			// Make the HTTP GET request in the background
			go func(url string) {
				_, err := http.Get(url)
				if err != nil {
					fmt.Println("Error making request:", err)
					return
				}
			}(requestURL)

			// Introduce the specified delay between requests
			time.Sleep(delay)
		}
		z++
		if z >= 100000 {
			break
		}
	}
}

//func main() {
//	fileContent, err := ioutil.ReadFile("socks4.txt")
//	if err != nil {
//		fmt.Println("Error reading file:", err)
//		return
//	}
//
//	// Split the content into lines and format as "socks4 ip port"
//	proxies := strings.Split(string(fileContent), "\n")
//
//	var formattedProxies []string
//	for _, proxy := range proxies {
//		// Split IP and port from the input and format them
//		parts := strings.Split(proxy, ":")
//		if len(parts) == 2 {
//			formattedProxy := fmt.Sprintf("socks4 %s %s", parts[0], parts[1])
//			formattedProxies = append(formattedProxies, formattedProxy)
//		}
//	}
//
//	// Write the formatted proxies to socks4_output.txt file
//	outputFilename := "socks4_output.txt"
//	err = ioutil.WriteFile(outputFilename, []byte(strings.Join(formattedProxies, "\n")), 0644)
//	if err != nil {
//		fmt.Println("Error writing to file:", err)
//		return
//	}
//
//	fmt.Printf("Formatted proxies written to %s\n", outputFilename)
//}
