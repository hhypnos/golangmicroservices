package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"sync"
	"math/rand"
)

// Configurable options
var (
	url       = "url"
	concurrency = 10  // Number of concurrent requests
	duration   = 10   // Duration of the test in seconds
)

func main() {
	var wg sync.WaitGroup

	// Start the test timer
	timer := make(chan bool)
	go func() {
		<-time.After(time.Duration(duration))
		timer <- true
	}()

	// Launch concurrent requests
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func() {
			defer wg.Done()
			for {
				makeRequest()
				select {
				case <-timer:
					return
				default:
				}
			}
		}()
	}
	// Wait for all requests to finish
	wg.Wait()

	fmt.Println("Stress test completed.")
}
var rame = 0 
func makeRequest() {
	randomNumber := rand.Intn(100000)
	var randomText string
	textLength := 5
	for i := 0; i < textLength; i++ {
		randomText += string(rand.Intn(26) + 97) // Generate lowercase letter (a-z)
	}
	
	completeUrl := fmt.Sprintf("%s?v=%d&text=%s", url, randomNumber, randomText)
	resp, err := http.Get(completeUrl)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	defer resp.Body.Close()

	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return
	}
rame+=1
if resp.StatusCode == http.StatusOK {
    fmt.Println("Request successful!",rame)
} else {
    fmt.Println("Request failed with status code:", resp.StatusCode)
}
	// You can add logic here to analyze the response (e.g., status code, response time)
}

