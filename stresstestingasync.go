package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	var z = 0
	apiURL := "https://api.ipify.org?format=text"
	response, err := http.Get(apiURL)
	if err != nil {
		fmt.Println("Error fetching IP:", err)
		return
	}
	defer response.Body.Close()

	ip, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading IP response:", err)
		return
	}

	fmt.Println("Your public IP address is:", string(ip))

	requestsPerBatch := 3500
	batchDelay := time.Second * 2
	for {
		batchStartTime := time.Now()

		for i := 0; i < requestsPerBatch; i++ {
			requestURL := fmt.Sprintf("%d", rand.Intn(90000)+10000)

			fmt.Println("Sending request to:", requestURL, i)

			client := &http.Client{}

			rrr, err := client.Get(requestURL)
			if err != nil {
				fmt.Println("Error making request:", err)
				continue
			}
			defer rrr.Body.Close()

			// Read response body
			body, err := ioutil.ReadAll(rrr.Body)
			if err != nil {
				fmt.Println("Error reading response body:", err)
				continue
			}

			// Print response body
			fmt.Println("Response:", string(body))

			time.Sleep(time.Millisecond * 10)
		}

		elapsed := time.Since(batchStartTime)
		if elapsed < batchDelay {
			time.Sleep(batchDelay - elapsed)
		}

		z++
		if z >= 100000 {
			break
		}
	}
}
